// Code generated by go-swagger; DO NOT EDIT.

package gameserver

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"fmt"
	"io"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"

	"gitlab.com/BIC_Dev/nitrado-api-client/models"
)

// FilesStatsReader is a Reader for the FilesStats structure.
type FilesStatsReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *FilesStatsReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewFilesStatsOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil
	case 401:
		result := NewFilesStatsUnauthorized()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	case 429:
		result := NewFilesStatsTooManyRequests()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	case 503:
		result := NewFilesStatsServiceUnavailable()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	default:
		result := NewFilesStatsDefault(response.Code())
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		if response.Code()/100 == 2 {
			return result, nil
		}
		return nil, result
	}
}

// NewFilesStatsOK creates a FilesStatsOK with default headers values
func NewFilesStatsOK() *FilesStatsOK {
	return &FilesStatsOK{}
}

/* FilesStatsOK describes a response with status code 200, with default header values.

success response
*/
type FilesStatsOK struct {
	Payload *FilesStatsOKBody
}

func (o *FilesStatsOK) Error() string {
	return fmt.Sprintf("[GET /services/{serviceID}/gameservers/file_server/stat][%d] filesStatsOK  %+v", 200, o.Payload)
}
func (o *FilesStatsOK) GetPayload() *FilesStatsOKBody {
	return o.Payload
}

func (o *FilesStatsOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(FilesStatsOKBody)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewFilesStatsUnauthorized creates a FilesStatsUnauthorized with default headers values
func NewFilesStatsUnauthorized() *FilesStatsUnauthorized {
	return &FilesStatsUnauthorized{}
}

/* FilesStatsUnauthorized describes a response with status code 401, with default header values.

The provided access token is not valid (anymore).
*/
type FilesStatsUnauthorized struct {
	Payload *models.ErrorModel
}

func (o *FilesStatsUnauthorized) Error() string {
	return fmt.Sprintf("[GET /services/{serviceID}/gameservers/file_server/stat][%d] filesStatsUnauthorized  %+v", 401, o.Payload)
}
func (o *FilesStatsUnauthorized) GetPayload() *models.ErrorModel {
	return o.Payload
}

func (o *FilesStatsUnauthorized) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.ErrorModel)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewFilesStatsTooManyRequests creates a FilesStatsTooManyRequests with default headers values
func NewFilesStatsTooManyRequests() *FilesStatsTooManyRequests {
	return &FilesStatsTooManyRequests{}
}

/* FilesStatsTooManyRequests describes a response with status code 429, with default header values.

The rate limit has been exceeded. Please contact our support if you have a legit reason get a higher rate limit.
*/
type FilesStatsTooManyRequests struct {
	Payload *models.ErrorModel
}

func (o *FilesStatsTooManyRequests) Error() string {
	return fmt.Sprintf("[GET /services/{serviceID}/gameservers/file_server/stat][%d] filesStatsTooManyRequests  %+v", 429, o.Payload)
}
func (o *FilesStatsTooManyRequests) GetPayload() *models.ErrorModel {
	return o.Payload
}

func (o *FilesStatsTooManyRequests) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.ErrorModel)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewFilesStatsServiceUnavailable creates a FilesStatsServiceUnavailable with default headers values
func NewFilesStatsServiceUnavailable() *FilesStatsServiceUnavailable {
	return &FilesStatsServiceUnavailable{}
}

/* FilesStatsServiceUnavailable describes a response with status code 503, with default header values.

Maintenance. API is currently not available. Please come back in a few minutes and try it again.
*/
type FilesStatsServiceUnavailable struct {
	Payload *models.ErrorModel
}

func (o *FilesStatsServiceUnavailable) Error() string {
	return fmt.Sprintf("[GET /services/{serviceID}/gameservers/file_server/stat][%d] filesStatsServiceUnavailable  %+v", 503, o.Payload)
}
func (o *FilesStatsServiceUnavailable) GetPayload() *models.ErrorModel {
	return o.Payload
}

func (o *FilesStatsServiceUnavailable) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.ErrorModel)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewFilesStatsDefault creates a FilesStatsDefault with default headers values
func NewFilesStatsDefault(code int) *FilesStatsDefault {
	return &FilesStatsDefault{
		_statusCode: code,
	}
}

/* FilesStatsDefault describes a response with status code -1, with default header values.

Default error response.
*/
type FilesStatsDefault struct {
	_statusCode int

	Payload *models.ErrorModel
}

// Code gets the status code for the files stats default response
func (o *FilesStatsDefault) Code() int {
	return o._statusCode
}

func (o *FilesStatsDefault) Error() string {
	return fmt.Sprintf("[GET /services/{serviceID}/gameservers/file_server/stat][%d] filesStats default  %+v", o._statusCode, o.Payload)
}
func (o *FilesStatsDefault) GetPayload() *models.ErrorModel {
	return o.Payload
}

func (o *FilesStatsDefault) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.ErrorModel)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

/*FilesStatsOKBody files stats o k body
swagger:model FilesStatsOKBody
*/
type FilesStatsOKBody struct {

	// data
	Data *FilesStatsOKBodyData `json:"data,omitempty"`

	// status
	Status string `json:"status,omitempty"`
}

// Validate validates this files stats o k body
func (o *FilesStatsOKBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateData(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *FilesStatsOKBody) validateData(formats strfmt.Registry) error {
	if swag.IsZero(o.Data) { // not required
		return nil
	}

	if o.Data != nil {
		if err := o.Data.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("filesStatsOK" + "." + "data")
			}
			return err
		}
	}

	return nil
}

// ContextValidate validate this files stats o k body based on the context it is used
func (o *FilesStatsOKBody) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	var res []error

	if err := o.contextValidateData(ctx, formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *FilesStatsOKBody) contextValidateData(ctx context.Context, formats strfmt.Registry) error {

	if o.Data != nil {
		if err := o.Data.ContextValidate(ctx, formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("filesStatsOK" + "." + "data")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *FilesStatsOKBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *FilesStatsOKBody) UnmarshalBinary(b []byte) error {
	var res FilesStatsOKBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

/*FilesStatsOKBodyData files stats o k body data
swagger:model FilesStatsOKBodyData
*/
type FilesStatsOKBodyData struct {

	// entries
	Entries map[string]FilesStatsOKBodyDataEntriesAnon `json:"entries,omitempty"`
}

// Validate validates this files stats o k body data
func (o *FilesStatsOKBodyData) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateEntries(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *FilesStatsOKBodyData) validateEntries(formats strfmt.Registry) error {
	if swag.IsZero(o.Entries) { // not required
		return nil
	}

	for k := range o.Entries {

		if swag.IsZero(o.Entries[k]) { // not required
			continue
		}
		if val, ok := o.Entries[k]; ok {
			if err := val.Validate(formats); err != nil {
				return err
			}
		}

	}

	return nil
}

// ContextValidate validate this files stats o k body data based on the context it is used
func (o *FilesStatsOKBodyData) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	var res []error

	if err := o.contextValidateEntries(ctx, formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *FilesStatsOKBodyData) contextValidateEntries(ctx context.Context, formats strfmt.Registry) error {

	for k := range o.Entries {

		if val, ok := o.Entries[k]; ok {
			if err := val.ContextValidate(ctx, formats); err != nil {
				return err
			}
		}

	}

	return nil
}

// MarshalBinary interface implementation
func (o *FilesStatsOKBodyData) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *FilesStatsOKBodyData) UnmarshalBinary(b []byte) error {
	var res FilesStatsOKBodyData
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

/*FilesStatsOKBodyDataEntriesAnon files stats o k body data entries anon
swagger:model FilesStatsOKBodyDataEntriesAnon
*/
type FilesStatsOKBodyDataEntriesAnon struct {

	// accessed at
	AccessedAt int32 `json:"accessed_at,omitempty"`

	// chmod
	Chmod string `json:"chmod,omitempty"`

	// created at
	CreatedAt int32 `json:"created_at,omitempty"`

	// group
	Group string `json:"group,omitempty"`

	// modified at
	ModifiedAt int32 `json:"modified_at,omitempty"`

	// name
	Name string `json:"name,omitempty"`

	// owner
	Owner string `json:"owner,omitempty"`

	// path
	Path string `json:"path,omitempty"`

	// size
	Size int32 `json:"size,omitempty"`

	// type
	Type string `json:"type,omitempty"`
}

// Validate validates this files stats o k body data entries anon
func (o *FilesStatsOKBodyDataEntriesAnon) Validate(formats strfmt.Registry) error {
	return nil
}

// ContextValidate validates this files stats o k body data entries anon based on context it is used
func (o *FilesStatsOKBodyDataEntriesAnon) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (o *FilesStatsOKBodyDataEntriesAnon) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *FilesStatsOKBodyDataEntriesAnon) UnmarshalBinary(b []byte) error {
	var res FilesStatsOKBodyDataEntriesAnon
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}
