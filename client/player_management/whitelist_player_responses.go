// Code generated by go-swagger; DO NOT EDIT.

package player_management

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"fmt"
	"io"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"

	"gitlab.com/BIC_Dev/nitrado-api-client/models"
)

// WhitelistPlayerReader is a Reader for the WhitelistPlayer structure.
type WhitelistPlayerReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *WhitelistPlayerReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewWhitelistPlayerOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil
	case 401:
		result := NewWhitelistPlayerUnauthorized()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	case 429:
		result := NewWhitelistPlayerTooManyRequests()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	case 503:
		result := NewWhitelistPlayerServiceUnavailable()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	default:
		result := NewWhitelistPlayerDefault(response.Code())
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		if response.Code()/100 == 2 {
			return result, nil
		}
		return nil, result
	}
}

// NewWhitelistPlayerOK creates a WhitelistPlayerOK with default headers values
func NewWhitelistPlayerOK() *WhitelistPlayerOK {
	return &WhitelistPlayerOK{}
}

/* WhitelistPlayerOK describes a response with status code 200, with default header values.

success response
*/
type WhitelistPlayerOK struct {
	Payload *WhitelistPlayerOKBody
}

func (o *WhitelistPlayerOK) Error() string {
	return fmt.Sprintf("[POST /services/{serviceID}/gameservers/games/whitelist][%d] whitelistPlayerOK  %+v", 200, o.Payload)
}
func (o *WhitelistPlayerOK) GetPayload() *WhitelistPlayerOKBody {
	return o.Payload
}

func (o *WhitelistPlayerOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(WhitelistPlayerOKBody)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewWhitelistPlayerUnauthorized creates a WhitelistPlayerUnauthorized with default headers values
func NewWhitelistPlayerUnauthorized() *WhitelistPlayerUnauthorized {
	return &WhitelistPlayerUnauthorized{}
}

/* WhitelistPlayerUnauthorized describes a response with status code 401, with default header values.

The provided access token is not valid (anymore).
*/
type WhitelistPlayerUnauthorized struct {
	Payload *models.ErrorModel
}

func (o *WhitelistPlayerUnauthorized) Error() string {
	return fmt.Sprintf("[POST /services/{serviceID}/gameservers/games/whitelist][%d] whitelistPlayerUnauthorized  %+v", 401, o.Payload)
}
func (o *WhitelistPlayerUnauthorized) GetPayload() *models.ErrorModel {
	return o.Payload
}

func (o *WhitelistPlayerUnauthorized) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.ErrorModel)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewWhitelistPlayerTooManyRequests creates a WhitelistPlayerTooManyRequests with default headers values
func NewWhitelistPlayerTooManyRequests() *WhitelistPlayerTooManyRequests {
	return &WhitelistPlayerTooManyRequests{}
}

/* WhitelistPlayerTooManyRequests describes a response with status code 429, with default header values.

The rate limit has been exceeded. Please contact our support if you have a legit reason get a higher rate limit.
*/
type WhitelistPlayerTooManyRequests struct {
	Payload *models.ErrorModel
}

func (o *WhitelistPlayerTooManyRequests) Error() string {
	return fmt.Sprintf("[POST /services/{serviceID}/gameservers/games/whitelist][%d] whitelistPlayerTooManyRequests  %+v", 429, o.Payload)
}
func (o *WhitelistPlayerTooManyRequests) GetPayload() *models.ErrorModel {
	return o.Payload
}

func (o *WhitelistPlayerTooManyRequests) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.ErrorModel)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewWhitelistPlayerServiceUnavailable creates a WhitelistPlayerServiceUnavailable with default headers values
func NewWhitelistPlayerServiceUnavailable() *WhitelistPlayerServiceUnavailable {
	return &WhitelistPlayerServiceUnavailable{}
}

/* WhitelistPlayerServiceUnavailable describes a response with status code 503, with default header values.

Maintenance. API is currently not available. Please come back in a few minutes and try it again.
*/
type WhitelistPlayerServiceUnavailable struct {
	Payload *models.ErrorModel
}

func (o *WhitelistPlayerServiceUnavailable) Error() string {
	return fmt.Sprintf("[POST /services/{serviceID}/gameservers/games/whitelist][%d] whitelistPlayerServiceUnavailable  %+v", 503, o.Payload)
}
func (o *WhitelistPlayerServiceUnavailable) GetPayload() *models.ErrorModel {
	return o.Payload
}

func (o *WhitelistPlayerServiceUnavailable) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.ErrorModel)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewWhitelistPlayerDefault creates a WhitelistPlayerDefault with default headers values
func NewWhitelistPlayerDefault(code int) *WhitelistPlayerDefault {
	return &WhitelistPlayerDefault{
		_statusCode: code,
	}
}

/* WhitelistPlayerDefault describes a response with status code -1, with default header values.

Default error response.
*/
type WhitelistPlayerDefault struct {
	_statusCode int

	Payload *models.ErrorModel
}

// Code gets the status code for the whitelist player default response
func (o *WhitelistPlayerDefault) Code() int {
	return o._statusCode
}

func (o *WhitelistPlayerDefault) Error() string {
	return fmt.Sprintf("[POST /services/{serviceID}/gameservers/games/whitelist][%d] whitelistPlayer default  %+v", o._statusCode, o.Payload)
}
func (o *WhitelistPlayerDefault) GetPayload() *models.ErrorModel {
	return o.Payload
}

func (o *WhitelistPlayerDefault) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.ErrorModel)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

/*WhitelistPlayerBody whitelist player body
swagger:model WhitelistPlayerBody
*/
type WhitelistPlayerBody struct {

	// identifier
	Identifier string `json:"identifier,omitempty"`
}

// Validate validates this whitelist player body
func (o *WhitelistPlayerBody) Validate(formats strfmt.Registry) error {
	return nil
}

// ContextValidate validates this whitelist player body based on context it is used
func (o *WhitelistPlayerBody) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (o *WhitelistPlayerBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *WhitelistPlayerBody) UnmarshalBinary(b []byte) error {
	var res WhitelistPlayerBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

/*WhitelistPlayerOKBody whitelist player o k body
swagger:model WhitelistPlayerOKBody
*/
type WhitelistPlayerOKBody struct {

	// message
	Message string `json:"message,omitempty"`

	// status
	Status string `json:"status,omitempty"`
}

// Validate validates this whitelist player o k body
func (o *WhitelistPlayerOKBody) Validate(formats strfmt.Registry) error {
	return nil
}

// ContextValidate validates this whitelist player o k body based on context it is used
func (o *WhitelistPlayerOKBody) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (o *WhitelistPlayerOKBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *WhitelistPlayerOKBody) UnmarshalBinary(b []byte) error {
	var res WhitelistPlayerOKBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}
