// Code generated by go-swagger; DO NOT EDIT.

package player_management

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/strfmt"
)

// NewUnwhitelistPlayerParams creates a new UnwhitelistPlayerParams object,
// with the default timeout for this client.
//
// Default values are not hydrated, since defaults are normally applied by the API server side.
//
// To enforce default values in parameter, use SetDefaults or WithDefaults.
func NewUnwhitelistPlayerParams() *UnwhitelistPlayerParams {
	return &UnwhitelistPlayerParams{
		timeout: cr.DefaultTimeout,
	}
}

// NewUnwhitelistPlayerParamsWithTimeout creates a new UnwhitelistPlayerParams object
// with the ability to set a timeout on a request.
func NewUnwhitelistPlayerParamsWithTimeout(timeout time.Duration) *UnwhitelistPlayerParams {
	return &UnwhitelistPlayerParams{
		timeout: timeout,
	}
}

// NewUnwhitelistPlayerParamsWithContext creates a new UnwhitelistPlayerParams object
// with the ability to set a context for a request.
func NewUnwhitelistPlayerParamsWithContext(ctx context.Context) *UnwhitelistPlayerParams {
	return &UnwhitelistPlayerParams{
		Context: ctx,
	}
}

// NewUnwhitelistPlayerParamsWithHTTPClient creates a new UnwhitelistPlayerParams object
// with the ability to set a custom HTTPClient for a request.
func NewUnwhitelistPlayerParamsWithHTTPClient(client *http.Client) *UnwhitelistPlayerParams {
	return &UnwhitelistPlayerParams{
		HTTPClient: client,
	}
}

/* UnwhitelistPlayerParams contains all the parameters to send to the API endpoint
   for the unwhitelist player operation.

   Typically these are written to a http.Request.
*/
type UnwhitelistPlayerParams struct {

	/* Body.

	   Unwhitelist a player body
	*/
	Body UnwhitelistPlayerBody

	/* ServiceID.

	   Service identifier
	*/
	ServiceID string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithDefaults hydrates default values in the unwhitelist player params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *UnwhitelistPlayerParams) WithDefaults() *UnwhitelistPlayerParams {
	o.SetDefaults()
	return o
}

// SetDefaults hydrates default values in the unwhitelist player params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *UnwhitelistPlayerParams) SetDefaults() {
	// no default values defined for this parameter
}

// WithTimeout adds the timeout to the unwhitelist player params
func (o *UnwhitelistPlayerParams) WithTimeout(timeout time.Duration) *UnwhitelistPlayerParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the unwhitelist player params
func (o *UnwhitelistPlayerParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the unwhitelist player params
func (o *UnwhitelistPlayerParams) WithContext(ctx context.Context) *UnwhitelistPlayerParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the unwhitelist player params
func (o *UnwhitelistPlayerParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the unwhitelist player params
func (o *UnwhitelistPlayerParams) WithHTTPClient(client *http.Client) *UnwhitelistPlayerParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the unwhitelist player params
func (o *UnwhitelistPlayerParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithBody adds the body to the unwhitelist player params
func (o *UnwhitelistPlayerParams) WithBody(body UnwhitelistPlayerBody) *UnwhitelistPlayerParams {
	o.SetBody(body)
	return o
}

// SetBody adds the body to the unwhitelist player params
func (o *UnwhitelistPlayerParams) SetBody(body UnwhitelistPlayerBody) {
	o.Body = body
}

// WithServiceID adds the serviceID to the unwhitelist player params
func (o *UnwhitelistPlayerParams) WithServiceID(serviceID string) *UnwhitelistPlayerParams {
	o.SetServiceID(serviceID)
	return o
}

// SetServiceID adds the serviceId to the unwhitelist player params
func (o *UnwhitelistPlayerParams) SetServiceID(serviceID string) {
	o.ServiceID = serviceID
}

// WriteToRequest writes these params to a swagger request
func (o *UnwhitelistPlayerParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error
	if err := r.SetBodyParam(o.Body); err != nil {
		return err
	}

	// path param serviceID
	if err := r.SetPathParam("serviceID", o.ServiceID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
