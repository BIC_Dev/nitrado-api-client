swagger generate client -A nitrado-api-client -c client -m models -f ~/go/src/gitlab.com/BIC_Dev/nitrado-api-client/swagger.yml -t ~/go/src/gitlab.com/BIC_Dev/nitrado-api-client
go mod init
go mod tidy
GOPROXY=https://proxy.golang.org GO111MODULE=on go get gitlab.com/BIC_Dev/nitrado-api-client@v1.0.0
curl https://sum.golang.org/lookup/gitlab.com/BIC_Dev/nitrado-api-client@1.0.0
