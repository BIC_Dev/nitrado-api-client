module gitlab.com/BIC_Dev/nitrado-api-client

go 1.16

require (
	github.com/go-openapi/errors v0.20.2
	github.com/go-openapi/runtime v0.23.2
	github.com/go-openapi/strfmt v0.21.2
	github.com/go-openapi/swag v0.21.1
)
